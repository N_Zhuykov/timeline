export const state = () =>({
    showNotification: false
})

export const mutations = {
    openNotification(state){
        state.showNotification = !state.showNotification
    }
}

export const actions ={
    OpenNotification(context){
        context.commit('openNotification')
    }
}